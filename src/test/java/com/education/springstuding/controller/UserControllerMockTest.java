package com.education.springstuding.controller;

import com.education.springstuding.model.User;
import com.education.springstuding.repository.UserRepository;
import com.education.springstuding.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerMockTest {

    @Mock
    Model model;
    @Mock
    User user;
    @Mock
    UserService userService;
    @InjectMocks
    UserController userController;

    @Test
    public void testUserList(){
        List<User> userList = new ArrayList<>();
        userList.add(user);
        when(userService.findAll()).thenReturn(userList);
        assertEquals("userList", userController.userList(model));
    }

    @Test
    public void testEditUser(){
        assertEquals("userEdit", userController.editUser(user, model));
    }

    @Test
    public void testUserSave(){
        Map<String, String> form = new HashMap<>();
        userService.saveUser(any(), anyString(), anyMap());
        assertEquals("redirect:/user", userController.userSave("test", form, user));
    }

    @Test
    public void testGetProfile(){
        assertEquals("profile", userController.getProfile(model, user));
    }

    @Test
    public void updateProfile(){
        userService.updateProfile(any(), anyString(), anyString());
        assertEquals("redirect:/user/profile", userController.updateProfile(user, "test", "test"));
    }
}
