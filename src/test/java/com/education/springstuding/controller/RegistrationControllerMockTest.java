package com.education.springstuding.controller;

import com.education.springstuding.model.User;
import com.education.springstuding.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationControllerMockTest {

    @Mock
    UserService userService;
    @Mock
    User user;
    @Mock
    Model model;
    @Mock
    BindingResult bindingResult;
    @InjectMocks
    RegistrationController registrationController;

    @Test
    public void testRegistration(){
        assertEquals("registration", registrationController.registration());
    }

    @Test
    public void testAddUserWnenUserIsNotInDb(){
        when(userService.addNewUser(any())).thenReturn(false);
        assertEquals("registration", registrationController.addUser(user, bindingResult, model));
    }

    @Test
    public void testAddUserWnenUserIsInDb(){
        when(userService.addNewUser(any())).thenReturn(true);
        assertEquals("redirect:/login", registrationController.addUser(user, bindingResult, model));
    }

    @Test
    public void testAddUserWnenBindingResultErrors(){
        when(bindingResult.hasErrors()).thenReturn(true);
        assertEquals("registration", registrationController.addUser(user, bindingResult, model));
    }

    @Test
    public void testActivateWhenUserActivated(){
        when(userService.activateUser(anyString())).thenReturn(true);
        assertEquals("/login", registrationController.activate("test", model));
    }

    @Test
    public void testActivateWhenUserNotActivated(){
        when(userService.activateUser(anyString())).thenReturn(false);
        assertEquals("/login", registrationController.activate("test", model));
    }
}
