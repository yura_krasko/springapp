package com.education.springstuding.controller;

import com.education.springstuding.model.Message;
import com.education.springstuding.model.User;
import com.education.springstuding.service.MessageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainControllerMockTest {

    @Mock
    MessageService messageService;
    @Mock
    Message message;
    @Mock
    Model model;
    @Mock
    User user;
    @Mock
    BindingResult bindingResult;
    @Mock
    MultipartFile file;
    @InjectMocks
    MainController mainController;

    @Test
    public void testMain(){
        assertEquals("index", mainController.main());
    }

    @Test
    public void testAllMessageWhenParamFiltrNotNull(){
        List<Message> messages = new ArrayList<>();
        messages.add(message);
        when(messageService.getMessageByTag(anyString())).thenReturn(messages);
        assertEquals("home", mainController.allMessage("test", model));
    }

    @Test
    public void testAllMessageWhenParamFiltrIsNullOrIsEmpty(){
        List<Message> messages = new ArrayList<>();
        messages.add(message);
        when(messageService.getAll()).thenReturn(messages);
        assertEquals("home", mainController.allMessage(null, model));
        assertEquals("home", mainController.allMessage("", model));
    }

    @Test
    public void testAddMessageWhenBindingResultErrors() throws IOException {
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(bindingResult.hasErrors()).thenReturn(true);
        when(messageService.getAll()).thenReturn(messageList);
        assertEquals("home", mainController.addMessage(user, message, bindingResult, model, file));
    }
}
