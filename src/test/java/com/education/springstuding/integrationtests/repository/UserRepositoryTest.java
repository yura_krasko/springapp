package com.education.springstuding.integrationtests.repository;

import com.education.springstuding.model.User;
import com.education.springstuding.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    UserRepository userRepository;

    User user;

    @Before
    public void setUp(){
        user = new User("test", "1111", true, "test@gmail.com", "1111");
    }

    @Test
    public void testFindByUsername(){
        entityManager.persist(user);
        User rezult = userRepository.findByUsername(user.getUsername());
        assertEquals(user.getUsername(), rezult.getUsername());
    }

    @Test
    public void testFindByActivationCode(){
        entityManager.persist(user);
        User result = userRepository.findByActivationCode(user.getActivationCode());
        assertEquals(user.getActivationCode(), result.getActivationCode());
    }
}
