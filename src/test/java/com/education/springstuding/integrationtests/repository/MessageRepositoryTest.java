package com.education.springstuding.integrationtests.repository;

import com.education.springstuding.model.Message;
import com.education.springstuding.model.User;
import com.education.springstuding.repository.MessageRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MessageRepositoryTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    MessageRepository messageRepository;

    Message message;
    User user;

    @Before
    public void setUp(){
        user = new User("test", "1111", true, "test@gmail.com", "1111");
        message = new Message("test", "tag", user, "testFile");
    }

    @Test
    public void findByTag(){
        entityManager.persist(user);
        entityManager.persist(message);
        List<Message> messages = messageRepository.findByTag("tag");
        assertEquals(1, messages.size());
    }
}
