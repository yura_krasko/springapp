package com.education.springstuding.integrationtests.service;

import com.education.springstuding.model.Message;
import com.education.springstuding.model.User;
import com.education.springstuding.repository.MessageRepository;
import com.education.springstuding.service.MessageService;
import com.education.springstuding.service.MessageServiceImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageServiseTest {

    @Autowired
    MessageServiceImpl messageService;

    @MockBean
    MessageRepository messageRepository;

    Message message;
    User user;

    @TestConfiguration
    static class MessageServiseImplTestContextConfiguration {
        @Bean
        public MessageService messageService() {
            return new MessageServiceImpl();
        }
    }

    @Before
    public void setUp(){
        user = new User("test", "1111", true, "test@gmail.com", "1111");
        message = new Message("test", "tag", user, "testFile");
    }

    @Test
    public void testGetAll(){
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(messageRepository.findAll()).thenReturn(messageList);
        List<Message> messages = messageService.getAll();
        assertEquals(1, messages.size());
    }

    @Test
    public void testAddNewMessage(){
        when(messageRepository.save(any())).thenReturn(message);
        Message addNewMessage = messageService.addNewMessage(message);
        assertEquals(message, addNewMessage);
    }

    @Test
    public void testGetMessageByTag(){
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(messageRepository.findByTag(anyString())).thenReturn(messageList);
        List<Message> messages = messageService.getMessageByTag("tag");
        assertEquals(messageList, messages);
    }
}
