package com.education.springstuding.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.internet.MimeMessage;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class MailSenderMockTest {

    @Mock
    JavaMailSender javaMailSender;
    @InjectMocks
    MailSender mailSender;
    @Mock
    SimpleMailMessage mailMessage;

    @Test(expected = NullPointerException.class)
    public void testSend(){
        mailMessage.setFrom("test");
        mailMessage.setTo("test");
        mailMessage.setSubject("test");
        mailMessage.setText("test");
        verify(javaMailSender).send(mailMessage);
        mailSender.sendMessage("test", "test", "test");
    }

}
