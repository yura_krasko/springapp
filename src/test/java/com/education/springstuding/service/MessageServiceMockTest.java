package com.education.springstuding.service;

import com.education.springstuding.model.Message;
import com.education.springstuding.repository.MessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceMockTest {

    @Mock
    MessageRepository messageRepository;
    @Mock
    Message message;
    @InjectMocks
    MessageServiceImpl messageService;

    @Test
    public void testGetAll(){
        List<Message> messages = new ArrayList<>();
        messages.add(message);
        when(messageRepository.findAll()).thenReturn(messages);
        assertEquals(1, messageService.getAll().size());
    }

    @Test
    public void testAddNewMessage(){
        when(messageRepository.save(any())).thenReturn(message);
        assertEquals(message, messageService.addNewMessage(message));
    }

    @Test
    public void testGetMessageByTag(){
        List<Message> messages = new ArrayList<>();
        messages.add(message);
        when(messageRepository.findByTag(anyString())).thenReturn(messages);
        assertEquals(messages, messageService.getMessageByTag("test"));
    }
}
