package com.education.springstuding.service;

import com.education.springstuding.model.Role;
import com.education.springstuding.model.User;
import com.education.springstuding.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceMockTest {

    @Mock
    UserRepository userRepository;
    @Mock
    MailSender mailSender;
    @Mock
    User user;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    Set<String> roles;
    @Mock
    Map<String, String> form;
    @InjectMocks
    UserServiceImpl userService;


    @Test
    public void testGetUserByName(){
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        assertEquals(user, userService.getUserByName("test"));
    }

    @Test
    public void testAddNewUserWhenResultFalse(){
        when(userRepository.findByUsername(any())).thenReturn(user);
        assertFalse( userService.addNewUser(user));
    }

    @Test
    public void testAddNewUserWhenResultTrue(){
        when(userRepository.findByUsername(any())).thenReturn(null);
        when(userRepository.save(any())).thenReturn(user);
        mailSender.sendMessage(anyString(), anyString(), anyString());
        assertTrue(userService.addNewUser(user));
    }

    @Test
    public void testLoadUserByUsernameWhenResultTrue(){
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        assertEquals(user, userService.loadUserByUsername("test"));
    }

    @Test(expected=UsernameNotFoundException.class)
    public void testLoadUserByUsernameWhenException(){
        when(userRepository.findByUsername(anyString())).thenReturn(null);
        assertEquals(UsernameNotFoundException.class, userService.loadUserByUsername("test"));
    }

    @Test
    public void activateUserWhenResultTrue(){
        when(userRepository.findByActivationCode(anyString())).thenReturn(user);
        assertTrue(userService.activateUser("test"));
    }

    @Test
    public void activateUserWhenResultFalse(){
        when(userRepository.findByActivationCode(anyString())).thenReturn(null);
        assertFalse(userService.activateUser("test"));
    }

    @Test
    public void testFindAll(){
        List<User> userList = new ArrayList<>();
        userList.add(user);
        when(userRepository.findAll()).thenReturn(userList);
        assertEquals(1, userService.findAll().size());
    }

    @Test
    public void testSaveUser(){
        when(userRepository.save(any())).thenReturn(user);
        userService.saveUser(user, "test", form);
    }

    @Test
    public void testUpdateProfile(){
        when(userRepository.save(any())).thenReturn(user);
        userService.updateProfile(user, passwordEncoder.encode("test"), "test");
    }
}
