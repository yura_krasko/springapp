<#import "parts/common.ftl" as common>
<@common.page>
<div class="form-row">
    <div class="form-group col-md-6">
        <form method="get" action="/home">
            <input type="text" name="filter" placeholder="Enter tag" value="${filter?if_exists}"/>
            <button type="submit" class="btn btn-primary c">Search</button>
        </form>
    </div>
</div>

<a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1"
   role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
    Add new message
</a>
<div class="collapse multi-collapse <#if message??>show</#if>" id="multiCollapseExample1">
    <form method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputTag" class="m-2 text-info">Tag</label>
                <input type="text" class="form-control ${(tagError??)?string('is-invalid', '')}"
                       name="tag" placeholder="Enter your tag"/>
                <#if tagError??>
                <div class="invalid-feedback">
                    ${tagError}
                </div>
                </#if>
            </div>
            <div class="form-group col-md-3">
                <label for="inputFile" class="m-2 text-info">File</label>
                <div class="custom-file">
                    <input type="file" name="file" id="inputFile"/>
                    <label class="custom-file-label" for="inputFile">Choose file</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="inputMessage" class="text-info">Message</label>
            <input type="text" class="form-control ${(textError??)?string('is-invalid', '')}"
                   value="<#if message??>${message.text}</#if>" name="text" placeholder="Enter your message"/>
            <#if textError??>
                <div class="invalid-feedback">
                    ${textError}
                </div>
            </#if>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Add message</button>
        </div>
    </form>
</div>

<div class="p-1 m-2" align="center">
    <h2>List messages</h2>
</div>

<div class="card-columns border-success mb-3" style="max-width: 108rem;">
    <#list messages as message>
        <div class="card border-info mb-3" style="max-width: 18rem;">
            <div class="card-header">Tag: #${message.tag}</div>
        <#if message.filename??>
        <img width="150" height="150" src="/img/${message.filename}" class="rounded mx-auto d-block">
        </#if>
            <div class="card-body m-2">
                <p class="card-text text-info">${message.text}</p>
            </div>
            <div class="card-footer bg-info text-white">
                Author:  ${message.authorName}
            </div>
        </div>
    <#else>
    No message
    </#list>
</div>
</@common.page>
