<#import "parts/common.ftl" as common>
<#import "parts/login.ftl" as login>
<@common.page>
<div class="col-form-label alert alert-success ml-5" align="center">
    <h2>Add New User</h2>
</div>
<div align="center"><h4>${message?if_exists}</h4></div>
    <@login.login "/registration" true/>
<div align="center">
    <a href="/login" class="btn btn-link">Login Page</a>
</div>
</@common.page>
