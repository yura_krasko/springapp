<#include "security.ftl">
<#import "logout.ftl" as logout>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Spring App</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav nav-tabs mr-auto">
            <li class="nav-item">
                <a class="nav-link active mr-3" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active mr-3" href="/home">Messages</a>
            </li>
            <#if isAdmin>
            <li class="nav-item">
                <a class="nav-link active mr-4" href="/user">User List</a>
            </li>
            </#if>
        </ul>
        <div class="navbar-text mr-2">${name}</div>
        <#if user??>
                <a class="btn btn-info mr-2" href="/user/profile">Profile</a>
            <@logout.logout/>
        </#if>
    </div>
</nav>