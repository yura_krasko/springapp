<#macro login path isRegisterForm=true>
    <form action="${path}" method="post">
        <div class="form-group" align="center">
            <label class="col-sm-2 col-form-label">User name</label>
            <div class="col-sm-3">
                <input type="text" name="username" value="<#if user??>${user.username}</#if>"
                       class="form-control ${(usernameError??)?string('is-invalid', '')}"
                       placeholder="Enter your user name"/ >
                 <#if usernameError??>
                <div class="invalid-feedback">
                    ${usernameError}
                </div>
                 </#if>
            </div>
        </div>
        <#if isRegisterForm>
        <div class="form-group" align="center">
            <label class="col-sm-2 col-form-label">Email address</label>
            <div class="col-sm-3">
                <input type="email" name="email" value="<#if user??>${user.email}</#if>"
                       class="form-control ${(emailError??)?string('is-invalid', '')}"
                       placeholder="example@example.com"/>
                <#if emailError??>
                <div class="invalid-feedback">
                    ${emailError}
                </div>
                </#if>
            </div>
        </div>
        </#if>
        <div class="form-group"  align="center">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-3">
                <input type="password"  name="password" value="<#if user??>${user.password}</#if>"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}" placeholder="Password">
                <#if passwordError??>
                <div class="invalid-feedback">
                    ${passwordError}
                </div>
                </#if>
            </div>
            <div class="form-group"  align="center">
                <label class="col-sm-2 col-form-label"></label>
                <div class="col-sm-3">
                    <input type="hidden" name="_csrf" value="${_csrf.token}" />
                    <button type="submit" class="btn btn-success btn-block" data-toggle="modal" data-target="#exampleModal">Sign In</button>
                    <#if isRegisterForm>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Activation account</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    We have sent a message to your email address, through which you can activate your account
                                </div>
                            </div>
                        </div>
                    </div>
                    </#if>
                </div>
            </div>
        </div>
    </form>
</#macro>