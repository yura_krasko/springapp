<#import "parts/common.ftl" as common>
<@common.page>
<div class="form-group row" align="center">
    <label for="staticEmail" class="col-sm-6 col-form-label alert alert-success ml-5">
        <h3>Edit user by id ${user.id}</h3></label>
</div>
<form action="/user" method="post">
    <div class="form-group row" for="validation">
        <label for="staticEmail" class="col-sm-2 col-form-label alert alert-primary ml-5">Edit user name:</label>
        <div class="col-sm-4">
            <input type="text" name="username" class="form-control" value="${user.username}" >
        </div>
    </div>
    <div class="form-group row" for="validation">
        <label for="staticEmail" class="col-sm-2 col-form-label alert alert-primary ml-5">Edit email:</label>
        <div class="col-sm-4">
            <input type="mail" name="email" class="form-control" value="${user.email}" >
        </div>
    </div>
    <div class="form-group row">
        <label for="staticEmail" class="col-sm-2 col-form-label alert alert-primary ml-5">Edit user role:</label>
        <div class="col-sm-4">
            <#list roles as role>
                <div>
                    <label><input type="checkbox" name="${role}" ${user.roles?seq_contains(role)?string("checked", "")}>${role}</label>
                </div>
            </#list>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-3">
            <input type="hidden" value="${user.id}" name="userId"/>
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <button type="submit" class="btn btn-success btn-block">Save</button>
            <a href="/home" class="btn btn-danger btn-block">Cancel</a>
        </div>
    </div>
</form>
</@common.page>