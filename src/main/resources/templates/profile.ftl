<#import "parts/common.ftl" as common>
<@common.page>
<div class="form-group row" align="center">
    <label for="staticEmail" class="col-sm-6 col-form-label alert alert-success ml-5"><h3>User: ${username}</h3></label>
</div>
    <form method="post">
        <div class="form-group row" for="validation">
            <label for="staticEmail" class="col-sm-2 col-form-label alert alert-primary ml-5">Edit email:</label>
            <div class="col-sm-4">
                <input type="email" name="email" class="form-control" value="${email!''}"
                       placeholder="example@example.com"/>
            </div>
        </div>
        <div class="form-group row" for="validation">
            <label for="staticEmail" class="col-sm-2 col-form-label alert alert-primary ml-5">Edit password:</label>
            <div class="col-sm-4">
                <input type="password"  name="password" class="form-control" placeholder="Password">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-3">
                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                <button type="submit" class="btn btn-success btn-block">Save</button>
                <a href="/home" class="btn btn-danger btn-block">Cancel</a>
            </div>
        </div>
    </form>
</@common.page>