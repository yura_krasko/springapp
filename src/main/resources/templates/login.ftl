<#import "parts/common.ftl" as common>
<#import "parts/login.ftl" as login>
<@common.page>
<div class="form-group" align="center">
    <#if Session?? && Session.SPRING_SECURITY_LAST_EXCEPTION??>
    <div class="col-sm-5 alert alert-danger" role="alert">
        ${Session.SPRING_SECURITY_LAST_EXCEPTION.message}
    </div>
    </#if>
    <#if message??>
    <div class="col-sm-5 alert alert-info" role="alert">
        ${message?if_exists}
    </div>
        </#if>
    <@login.login "/login" false/>
<div align="center">
    <a href="/registration" class="btn btn-link">Registration</a>
</div>
</div>
</@common.page>