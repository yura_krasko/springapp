<#import "parts/common.ftl" as common>
<@common.page>
<div class="p-1 m-2" align="center">
   <h2><p class="font-weight-bold">List of user</p></h2>
</div>
<table class="table table-bordered" align="center">
    <thead class="bg-primary text-white" align="center">
    <th scope="col">id user</th>
    <th scope="col">User name</th>
    <th scope="col">User active</th>
    <th scope="col">Email</th>
    <th scope="col">Role</th>
    <th scope="col">Edit</th>
    </thead>
    <tbody>
    <#list users as user>
    <tr scope="row" align="center">
        <td>${user.id}</td>
        <td>${user.username}</td>
        <td>${user.active?then('Yes', 'No')}</td>
        <td>${user.email}</td>
        <td><#list user.roles as role>${role}<#sep>, </#list></td>
        <td><a href="/user/${user.id}">Edit</a> </td>
    </tr>
    </#list>
    </tbody>
</table>
</@common.page>