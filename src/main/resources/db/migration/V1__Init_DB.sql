-- Create table message in database
create table message (
  id bigint not null auto_increment,
  filename varchar(255),
  tag varchar(255),
  text varchar(2048) not null,
  user_id bigint,
  primary key (id));

-- Create table user in database
create table user (
  user_id bigint not null auto_increment,
  activation_code varchar(255),
  active boolean,
  email varchar(255) not null ,
  password varchar(255) not null ,
  username varchar(255) not null ,
  primary key (user_id));


-- Create table user_role in database
create table user_role (
  user_id bigint not null auto_increment,
  roles integer);

alter table message
  add constraint message_user_fk
  foreign key (user_id) references user;

alter table user_role
  add constraint user_role_user_fk
  foreign key (user_id) references user;