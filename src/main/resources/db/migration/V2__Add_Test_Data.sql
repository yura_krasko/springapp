-- Test data for complete the table Message
insert into message (id, text, tag, user_id) values (1, 'hello world', 'hello', null);
insert into message (id, text, tag, user_id) values (2, 'hova are you', 'hello', null);
insert into message (id, text, tag, user_id) values (3, 'street', 'address', null);
insert into message (id, text, tag, user_id) values (4, 'town', 'address', null);
insert into message (id, text, tag, user_id) values (5, 'Your telephone', 'phone', null);

-- Test data for complete the table User
insert into user (user_id, username, password, active, email) values (1, 'admin', '$2a$08$hLCw1AFF4.27C5Zkt3k.AOpTAzPEX0rN6QNRoPKPGAhk.Bigf5Hoe', true, 'admin@ukr.net');
insert into user (user_id, username, password, active, email) values (2, 'user', '$2a$08$hLCw1AFF4.27C5Zkt3k.AOpTAzPEX0rN6QNRoPKPGAhk.Bigf5Hoe', true, 'user@ukr.net');

-- Test data for complete the table user_role
insert into user_role (user_id, roles) values (1, 1);
insert into user_role (user_id, roles) values (2, 0);