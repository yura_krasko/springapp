package com.education.springstuding.controller;

import com.education.springstuding.model.Message;
import com.education.springstuding.model.User;
import com.education.springstuding.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Controller
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    private final MessageService messageService;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    public MainController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/")
    public String main(){
        return "index";
    }

    @GetMapping("/home")
    public String allMessage (@RequestParam(required = false) String filter, final Model model){
        if (filter != null && !filter.isEmpty()){
            model.addAttribute("messages", messageService.getMessageByTag(filter));
        } else {
            model.addAttribute("messages", messageService.getAll());
        }
        model.addAttribute("filter", filter);
        return "home";
    }

    @PostMapping("/home")
    public String addMessage (@AuthenticationPrincipal User user,
                              @Valid Message message,
                              BindingResult bindingResult,
                              final Model model,
                              @RequestParam ("file") final MultipartFile file
    ) {
            message.setAuthor(user);
            if (bindingResult.hasErrors()) {
                Map<String, String> errorsMap = ControllerUtil.getErrors(bindingResult);
                model.mergeAttributes(errorsMap);
                model.addAttribute("message", message);
            } else {
                String resultFileName = saveFile(file);
                message.setFilename(resultFileName);
                model.addAttribute("message", null);
                messageService.addNewMessage(message);
            }
            model.addAttribute("messages", messageService.getAll());
            return "home";
    }


    private String saveFile(final MultipartFile file) {
        String uuidFile;
        String resultFileName = null;
        try {
            if (file != null & !file.getOriginalFilename().isEmpty()) {
                final File uploadDir = new File(uploadPath);
                if (!uploadDir.exists()) {
                    uploadDir.mkdir();
                }
                uuidFile = UUID.randomUUID().toString();
                resultFileName = uuidFile + "." + file.getOriginalFilename();
                file.transferTo(new File(uploadPath + "/" + resultFileName));
            }
        } catch (IOException e){
            logger.debug("Exception: " + e.getMessage());
        }
        return resultFileName;
    }
}
