package com.education.springstuding.repository;

import com.education.springstuding.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Get user by user name.
     * @param userName input parameter.
     * @return user from db.
     */
    User findByUsername(String userName);

    /**
     * Get user be activation code.
     * @param code input parameter.
     * @return user from db.
     */
    User findByActivationCode(String code);
}
