package com.education.springstuding.repository;

import com.education.springstuding.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    /**
     * Get list messages by tag.
     * @param tag input parameter.
     * @return list of messages.
     */
    List<Message> findByTag (String tag);
}
