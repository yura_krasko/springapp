package com.education.springstuding.service;

import com.education.springstuding.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    /**
     * Get user by userName.
     * @param userName input parameter.
     * @return user.
     */
    User getUserByName (final String userName);

    /**
     * Add new user.
     * @param user input parameter.
     * @return user.
     */
    Boolean addNewUser (final User user);

    /**
     * Activation user.
     * @param code input parameter.
     * @return result.
     */
    Boolean activateUser(final String code);

    void saveUser(User user, String userName, Map<String, String> form);

    List<User> findAll();

    void updateProfile(User user, String password, String email);
}
