package com.education.springstuding.service;

import com.education.springstuding.model.Message;

import java.util.List;

public interface MessageService {

    /**
     * Get all message from DB.
     * @return list of messages.
     */
    List<Message> getAll();

    /**
     * Add new message
     * @param message input parameter.
     * @return message.
     */
    Message addNewMessage (final Message message);

    /**
     * Get all message by tag.
     * @param tag input parameter.
     * @return list of messages.
     */
    List<Message> getMessageByTag(final String tag);
}
