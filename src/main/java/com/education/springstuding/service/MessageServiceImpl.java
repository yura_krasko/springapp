package com.education.springstuding.service;

import com.education.springstuding.model.Message;
import com.education.springstuding.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    /**
     * Implementation getting all messages from db.
     * @return list of messages.
     */
    @Override
    public List<Message> getAll() {
        return messageRepository.findAll();
    }

    /**
     * Implementation adding new message to db.
     * @param message input parameter.
     * @return message.
     */
    @Override
    public Message addNewMessage(final Message message) {
        return messageRepository.save(message);
    }

    /**
     * Implementation getting message by tag from db.
     * @param tag input parameter.
     * @return list of messages.
     */
    @Override
    public List<Message> getMessageByTag(final String tag) {
        return messageRepository.findByTag(tag);
    }
}
