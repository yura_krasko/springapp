package com.education.springstuding.service;

import com.education.springstuding.model.Role;
import com.education.springstuding.model.User;
import com.education.springstuding.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailSenderImp mailSender;

    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * Implementation getting user by name from db.     *
     * @param userName input parameter.
     * @return user.
     */
    @Override
    public User getUserByName(final String userName) {
        return userRepository.findByUsername(userName);
    }

    /**
     * Implementation adding new user to db.     *
     * @param user input parameter.
     * @return false when user not added, true when user added.
     */
    @Override
    public Boolean addNewUser(final User user) {
        User userFromDb = userRepository.findByUsername(user.getUsername());
        if (userFromDb != null){
            return false;
        }
        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        sendingActivationCode(user);
        return true;
    }

    /**
     * Implementation activation user.     *
     * @param code input parameter.
     * @return false when user not activated, true when user activated.
     */
    @Override
    public Boolean activateUser(final String code) {
        final User user = userRepository.findByActivationCode(code);
        if (user == null) {
            return false;
        }
        user.setActivationCode(null);
        userRepository.save(user);
        return true;
    }

    /**
     * Implementation getting user by user name from db.     *
     * @param username input parameter.
     * @return user.
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(final String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

    /**
     * Verification that email not null and sending activation code.     *
     * @param user input parameter.
     */
    private void sendingActivationCode(User user){
        if (!StringUtils.isEmpty(user.getEmail())){
            String message = String.format("Hello, %s! \n" +
                            "Welcome to SpringApp. From activation your account, please, visit next link http://localhost:8080/activate/%s",
                    user.getUsername(),
                    user.getActivationCode()
            );
            mailSender.sendMessage(user.getEmail(), "Activation code", message);
        }
    }

    @Override
    public void saveUser(User user, String userName, Map<String, String> form) {
        user.setUsername(userName);
        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());
        user.getRoles().clear();
        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }
        userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void updateProfile(User user, String password, String email) {
        String userEmail = user.getEmail();
        if (email != null && !email.equals(userEmail) || userEmail != null && !email.equals(userEmail)){
            user.setEmail(email);
            if (!StringUtils.isEmpty(email)){
                user.setActivationCode(UUID.randomUUID().toString());
            }
        }
        if (!StringUtils.isEmpty(password)){
            user.setPassword(passwordEncoder.encode(password));
        }
        userRepository.save(user);
    }
}
