package com.education.springstuding.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderImp implements MailSender{

    @Value("${spring.mail.username}")
    private String userName;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * Implementation sending mail message,
     * including data such as the from, to, cc, subject, and text fields.
     * @param mailTo input parameter.
     * @param subject input parameter.
     * @param message input parameter.
     */
    @Override
    public void sendMessage(final String mailTo, final String subject, final String message){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(userName);
        mailMessage.setTo(mailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
}
