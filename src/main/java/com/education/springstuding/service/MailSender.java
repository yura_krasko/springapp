package com.education.springstuding.service;

public interface MailSender {

    /**
     * Send message.
     *
     * @param mailTo input parameter.
     * @param subject input parameter.
     * @param message input parameter.
     */
    void sendMessage(final String mailTo, final String subject, final String message);
}
