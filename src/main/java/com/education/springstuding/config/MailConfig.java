package com.education.springstuding.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.username}")
    private String userName;

    @Value("${spring.mail.password}")
    private String password;

    @Value("${spring.mail.port}")
    private Integer port;

    @Value("${spring.mail.protocol}")
    private String protocol;

    private JavaMailSenderImpl mailSender;

    /**
     * Set JavaMail properties for creating bean.
     *
     * Set the mail server host.
     * Set the mail server port.
     * Set the mail user name.
     * Set the mail password.
     * Set the mail protocol.
     * @return JavaMail.
     */
    @Bean
    public JavaMailSender getMailsender(){
        mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(userName);
        mailSender.setPassword(password);
        mailSender.setProtocol(protocol);
        return mailSender;
    }
}
