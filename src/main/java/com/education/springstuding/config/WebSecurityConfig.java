package com.education.springstuding.config;

import com.education.springstuding.controller.MainController;
import com.education.springstuding.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Bean for encoder password.
     * @return password in hash format.
     */
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder(8);
    }

    /**
     * Config for http requests.
     *
     * For request "/", "/registration", "/activate/*" not need authorization and authentication.
     * For all others request need authorization and authentication.
     * For authorization and authentication used login form and page login.     *
     * @param http the instance to use.
     * @throws Exception
     */
    @Override
    protected void configure(final HttpSecurity http) {
        try {
            http
                    .authorizeRequests()
                    .antMatchers("/", "/registration", "/activate/*").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .and()
                    .logout()
                    .permitAll()
                    .and()
                    .exceptionHandling().accessDeniedPage("/user");
        } catch (Exception e) {
            logger.debug("Exception: " + e.getMessage());
        }
    }

    /**
     * Authentication users.
     *
     * Encoder for password uses the BCrypt hashing function.
     * @param auth the instance to use.
     * @throws Exception
     */
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) {
        try{
        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
         } catch (Exception e) {
            logger.debug("Exception: " + e.getMessage());
        }
    }
}
